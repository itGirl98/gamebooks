package gamebooks.gamebooks.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ListsFragment extends Fragment {

    private static final String API_KEY = "a2c66d52d6d126bbdb70dd0ff2220835";

    private Button button_triggerAPI;
    private View mView;
    public TextView mTextView;

    //private RequestQueue mRequestQueue;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //mRequestQueue = Volley.newRequestQueue(getContext());

        // Start the queue
        //mRequestQueue.start();
    }

    @Override
    public void onDestroy() {
        //mRequestQueue.stop();
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_lists, container, false);
        mTextView = (TextView) mView.findViewById(R.id.tv_lists);
        button_triggerAPI = (Button) mView.findViewById(R.id.button_triggerAPI);

        final TextView mTextView = (TextView) mView.findViewById(R.id.tv_lists);
        button_triggerAPI.setOnClickListener(this::onClick);

        return mView;
    }

    private void onClick(View view) {
        // user-key: a2c66d52d6d126bbdb70dd0ff2220835
        // Accept: application/json

        //getGames.getGames();


//        JsonArrayRequest request = new JsonArrayRequest(
//                "https://api-endpoint.igdb.com/games/",
//                (response) -> {
//                    System.out.println("Response: " + response);
//                    mTextView.setText(response.toString());
//                },
//                (error) -> {
//                    System.out.println("Error: " + error);
//                    mTextView.setText("Error: " + error);
//                }
//        ) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> headers = new HashMap<>();
//                headers.put("user-key", "a2c66d52d6d126bbdb70dd0ff2220835");
//                headers.put("Accept", "application/json");
//                return headers;
//            }
//        };

        List<Integer> IDs = new ArrayList<Integer>();

        JsonArrayRequest request = new JsonArrayRequest(
                "https://api-endpoint.igdb.com/games/",
                (response) -> {
                    System.out.println("Response: " + response);
                    mTextView.setText(response.toString());

                    for (int i = 0; i < response.length(); ++i){
                        try {
                            JSONObject jsonIDs= response.getJSONObject(i);
                            int id = jsonIDs.getInt("id");

                            getGameInfo(id);

                            IDs.add(id);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                (error) -> {
                    System.out.println("Error: " + error);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("user-key", "a2c66d52d6d126bbdb70dd0ff2220835");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        MainActivity.mRequestQueue.add(request);
        //mRequestQueue.add(request);

    }
    public void getGameInfo(int id) {

        Game game = new Game(id);

        JsonArrayRequest request = new JsonArrayRequest(
                "https://api-endpoint.igdb.com/games/" + id,
                (response) -> {
                    try {
                        JSONObject jsonObject = response.getJSONObject(0);

                        game.setID(jsonObject.getInt("id"));
                        game.setGameName(jsonObject.getString("name"));
                        game.setSlug(jsonObject.getString("slug"));
                        game.setUrl(jsonObject.getString("url"));
                        game.setCreatedAt(jsonObject.getString("created_at"));
                        game.setUpdatedAt(jsonObject.getString("updated_at"));
                        game.setSummary(jsonObject.getString("summary"));
                        game.setCollection(jsonObject.getInt("collection"));
                        game.setPopularity(jsonObject.getInt("popularity"));
                        game.setCategory(jsonObject.getInt("category"));


                        mTextView.setText(game.url);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                },
                (error) -> {
                    System.out.println("Error: " + error);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("user-key", "a2c66d52d6d126bbdb70dd0ff2220835");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        // gameList.add(game);

        MainActivity.mRequestQueue.add(request);
    }


}

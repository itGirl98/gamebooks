package gamebooks.gamebooks.app;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class getGames {

    List<Game> gameList = new ArrayList<>();


    public void getGames(){

        List<Integer> IDs = new ArrayList<Integer>();

        JsonArrayRequest request = new JsonArrayRequest(
                "https://api-endpoint.igdb.com/games/",
                (response) -> {
                    System.out.println("Response: " + response);

                    for (int i = 0; i < response.length(); ++i){
                        try {
                            JSONObject jsonIDs= response.getJSONObject(i);
                            int id = jsonIDs.getInt("id");

                            getGameInfo(id);

                            IDs.add(id);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                (error) -> {
                    System.out.println("Error: " + error);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("user-key", "a2c66d52d6d126bbdb70dd0ff2220835");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        MainActivity.mRequestQueue.add(request);
    }

    public void getGameInfo(int id){

        Game game = new Game(id);

        JsonArrayRequest request = new JsonArrayRequest(
                "https://api-endpoint.igdb.com/games/" + id,
                (response) -> {
                    try {
                        JSONObject jsonObject = response.getJSONObject(id);
                        System.out.println(game);

                        game.setID(jsonObject.getInt("id"));
                        game.setGameName(jsonObject.getString("name"));
                        game.setSlug(jsonObject.getString("slug"));
                        game.setUrl(jsonObject.getString("url"));
                        game.setCreatedAt(jsonObject.getString("created_at"));
                        game.setUpdatedAt(jsonObject.getString("updated_at"));
                        game.setSummary(jsonObject.getString("summary"));
                        game.setCollection(jsonObject.getInt("collection"));
                        game.setPopularity(jsonObject.getInt("popularity"));
                        game.setCategory(jsonObject.getInt("category"));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                },
                (error) -> {
                    System.out.println("Error: " + error);
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("user-key", "a2c66d52d6d126bbdb70dd0ff2220835");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        gameList.add(game);

        MainActivity.mRequestQueue.add(request);
    }
}

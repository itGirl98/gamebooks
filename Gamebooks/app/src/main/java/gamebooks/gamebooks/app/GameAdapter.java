package gamebooks.gamebooks.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class GameAdapter extends RecyclerView.Adapter<GameAdapter.MyViewHolder> {
    private String[] mGameNames;
    private String[] mGameUrls;
    private String[] mUpdatedAt;
    private String[] mCreatedAt;
    private Double[] mGameRatings;
    private String[] mImageUrls;
    private ImageLoader mImageLoader;

    private Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView mCardView;
        public TextView textViewTitle, textViewUrls, textViewCreated, textViewUpdated;
        public NetworkImageView cover;
        public ImageView overflow;
        public RatingBar ratingBar;

        public MyViewHolder(View v) {
            super(v);

            mCardView = (CardView) v.findViewById(R.id.card_view);
            textViewTitle = (TextView) v.findViewById(R.id.tv_title);
            ratingBar = (RatingBar) v.findViewById(R.id.rb_gameRating);
            textViewUrls = (TextView) v.findViewById(R.id.tv_urls);
            textViewCreated = (TextView) v.findViewById(R.id.tv_createdAt);
            textViewUpdated = (TextView) v.findViewById(R.id.tv_updatedAt);
            cover = (NetworkImageView) v.findViewById(R.id.iv_cover);
            overflow = (ImageView) v.findViewById(R.id.overflow);

            ratingBar.setEnabled(false);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public GameAdapter(
            Context mContext,
            String[] gameNames,
            Double[] gameRatings,
            String[] gameUrls,
            String[] createdAt,
            String[] updatedAt,
            String[] imageUrls
    ) {
        this.mContext = mContext;
        this.mGameNames = gameNames;
        this.mGameUrls = gameUrls;
        this.mCreatedAt = createdAt;
        this.mUpdatedAt = updatedAt;
        this.mGameRatings = gameRatings;
        this.mImageUrls = imageUrls;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public GameAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                       int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        mImageLoader = new ImageLoader(Volley.newRequestQueue(mContext),
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }


                })
        {
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("user-key", "a2c66d52d6d126bbdb70dd0ff2220835");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        float[] floatArray = new float[mGameRatings.length];
        for (int i = 0 ; i < mGameRatings.length; i++)
        {
            floatArray[i] = mGameRatings[i].floatValue();
        }


        holder.textViewTitle.setText(mGameNames[position]);
        holder.textViewUrls.setText(mGameUrls[position]);
        holder.ratingBar.setRating(floatArray[position]);
        holder.textViewCreated.setText(mCreatedAt[position]);
        holder.textViewUpdated.setText(mUpdatedAt[position]);

        holder.cover.setImageUrl(mImageUrls[position], mImageLoader);


        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow);
            }
        });
    }

    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_overflow, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_share:
                    Toast.makeText(mContext, "Share", Toast.LENGTH_SHORT).show();
return true;
                default:
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return mGameNames.length;
    }


}
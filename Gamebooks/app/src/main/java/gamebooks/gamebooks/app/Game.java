package gamebooks.gamebooks.app;

import java.io.Serializable;
import java.util.List;

public class Game implements Serializable {

    List<Object> games;
    Integer ID;
    String gameName;
    String slug;
    String url;
    String createdAt;
    String updatedAt;
    String summary;
    String storyline;
    Integer collection;
    Integer franchise;
    Integer hypes;
    Integer popularity;
    Double rating;
    Integer ratingCount;
    Double aggregatedRating;
    Integer aggregatedRatingCount;
    Double totalRating;
    Integer totalRatingCount;
    Integer gameID;
    Integer versionParentID;
    Integer[] developersID;
    Integer[] publishersID;
    Integer[] gameEnginesID;
    Integer category;
    Object TimeToBeat;
    Integer[] playerPerspectivesID;
    Integer[] gameModesID;
    Integer[] keywordsID;
    Integer[] themesID;
    Integer[] genresID;
    Integer[] platformsID;
    Integer firstReleaseDate;
    Integer status;
    Object[] releaseDates;
    Object[] alternativeNames;
    Object[] screenshots;
    Object[] videos;
    Object cover;
    Object esrb;
    Object pegi;
    Object[] websites;
    Integer[] tags;
    Integer[] dlcsID;
    Integer[] expansionsID;
    Integer[] standaloneExpansionsID;
    Integer[] bundlesID;
    Integer[] gamesID;
    Object external;
    Object artwork;

    public Game(){}

    public Game(Integer id){
        ID = getID();
        gameName = getGameName();
        slug = getSlug();
        url = getUrl();
        createdAt = getCreatedAt();
        updatedAt = getUpdatedAt();
        summary = getSummary();
        storyline = getStoryline();
        franchise = getFranchise();
        hypes = getHypes();
        popularity = getPopularity();
        rating = getRating();
        ratingCount = getRatingCount();
        aggregatedRating = getAggregatedRating();
        aggregatedRatingCount = getAggregatedRatingCount();
        totalRating = getTotalRating();
        totalRatingCount = getTotalRatingCount();
        gameID = getGameID();
        versionParentID = getVersionParentID();
        developersID = getDevelopersID();
        publishersID = getPublishersID();
        gameEnginesID = getGameEnginesID();
        category = getCategory();
        TimeToBeat = getTimeToBeat();
        playerPerspectivesID = getPlayerPerspectivesID();
        gameModesID = getGameModesID();
        keywordsID = getKeywordsID();
        themesID = getThemesID();
        genresID = getGenresID();
        platformsID = getPlatformsID();
        firstReleaseDate = getFirstReleaseDate();
        status = getStatus();
        releaseDates = getReleaseDates();
        alternativeNames = getAlternativeNames();
        screenshots = getScreenshots();
        videos = getVideos();
        cover = getCover();
        esrb = getEsrb();
        pegi = getPegi();
        websites = getWebsites();
        tags = getTags();
        dlcsID = getDlcsID();
        expansionsID = getExpansionsID();
        standaloneExpansionsID = getStandaloneExpansionsID();
        bundlesID = getBundlesID();
        gamesID = getGamesID();
        external = getExternal();
        artwork = getArtwork();
    }

    public List<Object> getGames() {
        return games;
    }

    public void setGames(List<Object> games) {
        this.games = games;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getStoryline() {
        return storyline;
    }

    public void setStoryline(String storyline) {
        this.storyline = storyline;
    }

    public Integer getCollection() {
        return collection;
    }

    public void setCollection(Integer collection) {
        this.collection = collection;
    }

    public Integer getFranchise() {
        return franchise;
    }

    public void setFranchise(Integer franchise) {
        this.franchise = franchise;
    }

    public Integer getHypes() {
        return hypes;
    }

    public void setHypes(Integer hypes) {
        this.hypes = hypes;
    }

    public Integer getPopularity() {
        return popularity;
    }

    public void setPopularity(Integer popularity) {
        this.popularity = popularity;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(Integer ratingCount) {
        this.ratingCount = ratingCount;
    }

    public Double getAggregatedRating() {
        return aggregatedRating;
    }

    public void setAggregatedRating(Double aggregatedRating) {
        this.aggregatedRating = aggregatedRating;
    }

    public Integer getAggregatedRatingCount() {
        return aggregatedRatingCount;
    }

    public void setAggregatedRatingCount(Integer aggregatedRatingCount) {
        this.aggregatedRatingCount = aggregatedRatingCount;
    }

    public Double getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(Double totalRating) {
        this.totalRating = totalRating;
    }

    public Integer getTotalRatingCount() {
        return totalRatingCount;
    }

    public void setTotalRatingCount(Integer totalRatingCount) {
        this.totalRatingCount = totalRatingCount;
    }

    public Integer getGameID() {
        return gameID;
    }

    public void setGameID(Integer gameID) {
        this.gameID = gameID;
    }

    public Integer getVersionParentID() {
        return versionParentID;
    }

    public void setVersionParentID(Integer versionParentID) {
        this.versionParentID = versionParentID;
    }

    public Integer[] getDevelopersID() {
        return developersID;
    }

    public void setDevelopersID(Integer[] developersID) {
        this.developersID = developersID;
    }

    public Integer[] getPublishersID() {
        return publishersID;
    }

    public void setPublishersID(Integer[] publishersID) {
        this.publishersID = publishersID;
    }

    public Integer[] getGameEnginesID() {
        return gameEnginesID;
    }

    public void setGameEnginesID(Integer[] gameEnginesID) {
        this.gameEnginesID = gameEnginesID;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Object getTimeToBeat() {
        return TimeToBeat;
    }

    public void setTimeToBeat(Object timeToBeat) {
        TimeToBeat = timeToBeat;
    }

    public Integer[] getPlayerPerspectivesID() {
        return playerPerspectivesID;
    }

    public void setPlayerPerspectivesID(Integer[] playerPerspectivesID) {
        this.playerPerspectivesID = playerPerspectivesID;
    }

    public Integer[] getGameModesID() {
        return gameModesID;
    }

    public void setGameModesID(Integer[] gameModesID) {
        this.gameModesID = gameModesID;
    }

    public Integer[] getKeywordsID() {
        return keywordsID;
    }

    public void setKeywordsID(Integer[] keywordsID) {
        this.keywordsID = keywordsID;
    }


    public Integer[] getThemesID() {
        return themesID;
    }

    public void setThemesID(Integer[] themesID) {
        this.themesID = themesID;
    }

    public Integer[] getGenresID() {
        return genresID;
    }

    public void setGenresID(Integer[] genresID) {
        this.genresID = genresID;
    }

    public Integer[] getPlatformsID() {
        return platformsID;
    }

    public void setPlatformsID(Integer[] platformsID) {
        this.platformsID = platformsID;
    }

    public Integer getFirstReleaseDate() {
        return firstReleaseDate;
    }

    public void setFirstReleaseDate(Integer firstReleaseDate) {
        this.firstReleaseDate = firstReleaseDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object[] getReleaseDates() {
        return releaseDates;
    }

    public void setReleaseDates(Object[] releaseDates) {
        this.releaseDates = releaseDates;
    }

    public Object[] getAlternativeNames() {
        return alternativeNames;
    }

    public void setAlternativeNames(Object[] alternativeNames) {
        this.alternativeNames = alternativeNames;
    }

    public Object[] getScreenshots() {
        return screenshots;
    }

    public void setScreenshots(Object[] screenshots) {
        this.screenshots = screenshots;
    }

    public Object[] getVideos() {
        return videos;
    }

    public void setVideos(Object[] videos) {
        this.videos = videos;
    }

    public Object getCover() {
        return cover;
    }

    public void setCover(Object cover) {
        this.cover = cover;
    }

    public Object getEsrb() {
        return esrb;
    }

    public void setEsrb(Object esrb) {
        this.esrb = esrb;
    }

    public Object getPegi() {
        return pegi;
    }

    public void setPegi(Object pegi) {
        this.pegi = pegi;
    }

    public Object[] getWebsites() {
        return websites;
    }

    public void setWebsites(Object[] websites) {
        this.websites = websites;
    }

    public Integer[] getTags() {
        return tags;
    }

    public void setTags(Integer[] tags) {
        this.tags = tags;
    }

    public Integer[] getDlcsID() {
        return dlcsID;
    }

    public void setDlcsID(Integer[] dlcsID) {
        this.dlcsID = dlcsID;
    }

    public Integer[] getExpansionsID() {
        return expansionsID;
    }

    public void setExpansionsID(Integer[] expansionsID) {
        this.expansionsID = expansionsID;
    }

    public Integer[] getStandaloneExpansionsID() {
        return standaloneExpansionsID;
    }

    public void setStandaloneExpansionsID(Integer[] standaloneExpansionsID) {
        this.standaloneExpansionsID = standaloneExpansionsID;
    }

    public Integer[] getBundlesID() {
        return bundlesID;
    }

    public void setBundlesID(Integer[] bundlesID) {
        this.bundlesID = bundlesID;
    }

    public Integer[] getGamesID() {
        return gamesID;
    }

    public void setGamesID(Integer[] gamesID) {
        this.gamesID = gamesID;
    }

    public Object getExternal() {
        return external;
    }

    public void setExternal(Object external) {
        this.external = external;
    }

    public Object getArtwork() {
        return artwork;
    }

    public void setArtwork(Object artwork) {
        this.artwork = artwork;
    }
}

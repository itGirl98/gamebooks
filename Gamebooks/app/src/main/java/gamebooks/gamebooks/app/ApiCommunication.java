package gamebooks.gamebooks.app;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import static android.support.test.InstrumentationRegistry.getContext;

public class ApiCommunication {

    private final static RequestQueue mRequestQueue;

    private ApiCommunication(){}

    static {
        // Instantiate the cache
        mRequestQueue = Volley.newRequestQueue(getContext());

        // Start the queue
        mRequestQueue.start();
    }

    private static final String API_KEY = "a2c66d52d6d126bbdb70dd0ff2220835";


    public static void getGamesfromAPI(Response.Listener<JSONObject> responseCallback, Response.ErrorListener errorCallback) {

        JsonObjectRequest request = new JsonObjectRequest(
                "https://api-endpoint.igdb.com/games/", null,
                responseCallback, errorCallback
        );

        mRequestQueue.add(request);
    }
}


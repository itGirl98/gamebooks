package gamebooks.gamebooks.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeFragment extends Fragment {

    Game game;
    List<Game> gameList;
    List<String> gameNames;
    List<Integer> IDs;
    View rootView;
    RecyclerView rv;
    List<String> gameUrls;
    List<String> summarys;
    List<String> createdAt;
    List<String> updatedAt;
    List<Double> gameRatings;
    View mLoadingPanel;
    List<String> image_urls;

    public HomeFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView( LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {

        IDs = new ArrayList<Integer>();
        gameList = new ArrayList<>();

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        rv = (RecyclerView) rootView.findViewById(R.id.rv_recycler_view);
        rv.setHasFixedSize(true);

        mLoadingPanel = (View) rootView.findViewById(R.id.loadingPanel);

        mLoadingPanel.setVisibility(View.VISIBLE);

        JsonArrayRequest request = new JsonArrayRequest(
                "https://api-endpoint.igdb.com/games/",
                (response) -> {
                    System.out.println("Response: " + response);

                    for (int i = 0; i < response.length(); ++i){
                        try {
                            JSONObject jsonIDs= response.getJSONObject(i);
                            int id = jsonIDs.getInt("id");

                            getGameInfo(id);
                            IDs.add(id);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                (error) -> {
                    System.out.println("Error: " + error);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("user-key", "a2c66d52d6d126bbdb70dd0ff2220835");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        MainActivity.mRequestQueue.add(request);
        return rootView;

    }


    public void getGameInfo(int id) {

        gameNames = new ArrayList<>();
        gameUrls = new ArrayList<>();
        summarys = new ArrayList<>();
        createdAt = new ArrayList<>();
        updatedAt = new ArrayList<>();
        gameRatings = new ArrayList<>();
        image_urls = new ArrayList<>();

            JsonArrayRequest requestGameInfo = new JsonArrayRequest(
                    "https://api-endpoint.igdb.com/games/" + id,
                    (response) -> {
                        try {
                            JSONObject jsonObject = response.getJSONObject(0);
                            Game newGame = new Game();

                            newGame.setID(jsonObject.getInt("id"));
                            newGame.setGameName(jsonObject.getString("name"));
                            newGame.setSlug(jsonObject.getString("slug"));

                            if(jsonObject.has("url")){
                                newGame.setUrl(jsonObject.getString("url"));
                                gameUrls.add(newGame.url);
                            }
                            else gameUrls.add("no url");

                            if(jsonObject.has("created_at")){
                                int created_at = jsonObject.getInt("created_at");
                                String date = Date.from( Instant.ofEpochSecond( created_at )).toString();
                                newGame.setCreatedAt(date);
                                createdAt.add(newGame.createdAt);
                            }
                            else createdAt.add("no created at");

                            if(jsonObject.has("updated_at")){
                                int updated_at = jsonObject.getInt("updated_at");
                                String date = Date.from( Instant.ofEpochSecond( updated_at )).toString();
                                newGame.setUpdatedAt(date);
                                updatedAt.add(newGame.updatedAt);
                            }
                            else updatedAt.add("no updated at");

                            if(jsonObject.has("summary")){
                                newGame.setSummary(jsonObject.getString("summary"));
                                summarys.add(newGame.summary);
                            }
                            else summarys.add("no summary");

                            if(jsonObject.has("rating")){
                                newGame.setRating(jsonObject.getDouble("rating"));
                                gameRatings.add(newGame.rating);
                            }
                            else gameRatings.add((double) -1);

                            if(jsonObject.has("cover")){

                                Object cover = jsonObject.get("cover");
                                newGame.setCover(cover);

                                String image_url = jsonObject.getJSONObject("cover").getString("url");
                                image_urls.add("http:" + image_url);
                            }
                            else image_urls.add("http://denrakaev.com/wp-content/uploads/2015/03/no-image.png");


                            //game.setStoryline(jsonObject.getString("storyline"));
//                            newGame.setCollection(jsonObject.getInt("collection"));
                            //game.setFranchise(jsonObject.getInt("franchise"));
                            //game.setHypes(jsonObject.getInt("hypes"));
//                            newGame.setPopularity(jsonObject.getInt("popularity"));
                            //game.setRatingCount(jsonObject.getInt("rating_count"));
                            //game.setAggregatedRating(jsonObject.getDouble("aggregated_rating"));
                            //game.setAggregatedRatingCount(jsonObject.getInt("aggregated_rating_count"));
                            //game.setTotalRating(jsonObject.getDouble("total_rating"));
                            //game.setTotalRatingCount(jsonObject.getInt("total_rating_count"));
                            //game.setGameID(jsonObject.getInt("game"));
                            //game.setVersionParentID(jsonObject.getInt("version_parent"));
                            //game.setDevelopersID(((Integer[]) jsonObject.get("developers")));
                            //game.setPublishersID(((Integer[]) jsonObject.get("publishers")));
                            //game.setGameEnginesID(((Integer[]) jsonObject.get("game_engines")));
//                            newGame.setCategory(jsonObject.getInt("category"));
                            //game.setTimeToBeat(jsonObject.get("time_to_beat"));
                            //game.setPlayerPerspectivesID(((Integer[]) jsonObject.get("player_perspectives")));
                            //game.setGameModesID(((Integer[]) jsonObject.get("game_modes")));
//                            game.setKeywordsID(((Integer[]) jsonObject.get("keywords")));
//                            game.setThemesID(((Integer[]) jsonObject.get("themes")));
//                            game.setGenresID(((Integer[]) jsonObject.get("genres")));
                            //game.setPlatformsID(((Integer[]) jsonObject.get("platforms")));
                            //game.setFirstReleaseDate(jsonObject.getInt("first_release_date"));
                            //game.setStatus(jsonObject.getInt("status"));
                            //game.setReleaseDates(((Object[]) jsonObject.get("release_dates")));
                            //game.setAlternativeNames(((Object[]) jsonObject.get("alternative_names")));
                            //game.setScreenshots(((Object[]) jsonObject.get("screenshots")));
                            //game.setVideos(((Object[]) jsonObject.get("videos")));
//                            newGame.setCover(jsonObject.get("cover"));
//                            newGame.setEsrb(jsonObject.get("esrb"));
                            //game.setPegi(jsonObject.get("pegi"));
                            //game.setWebsites(((Object[]) jsonObject.get("websites")));
                            //game.setTags(((Integer[]) jsonObject.get("tags")));
                            //game.setDlcsID(((Integer[]) jsonObject.get("dlcs")));
                            //game.setExpansionsID(((Integer[]) jsonObject.get("expansions")));
                            //game.setStandaloneExpansionsID(((Integer[]) jsonObject.get("standalone_expansions")));
                            //game.setBundlesID(((Integer[]) jsonObject.get("bundles")));
                            //game.setGamesID(((Integer[]) jsonObject.get("games")));
                            //game.setExternal(jsonObject.get("external"));
                            //game.setArtwork(jsonObject.get("artwork"));

                            gameList.add(newGame);
                            gameNames.add(newGame.gameName);

                            if(IDs.size() == gameList.size() && IDs.size() != 0){

                                String[] gameNameArray = gameNames.toArray(new String[0]);
                                String[] gameUrlsArray = gameUrls.toArray(new String[0]);
                                String[] createdAtArray = createdAt.toArray(new String[0]);
                                String[] updatedAtArray = updatedAt.toArray(new String[0]);
                                String[] gameSummarysArray = summarys.toArray(new String[0]);
                                Double[] gameRatingArray = gameRatings.toArray(new Double[0]);
                                String[] imageUrlsArray = image_urls.toArray(new String[0]);

                                GameAdapter gameAdapter = new GameAdapter(
                                        getContext(),
                                        gameNameArray,
                                        gameRatingArray,
                                        gameUrlsArray,
                                        createdAtArray,
                                        updatedAtArray,
                                        imageUrlsArray);
                                rv.setAdapter(gameAdapter);
                                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                                rv.setLayoutManager(llm);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        mLoadingPanel.setVisibility(View.GONE);

                    },
                    (error) -> {
                        System.out.println("Error: " + error);
                    }

            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("user-key", "a2c66d52d6d126bbdb70dd0ff2220835");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            //gameList.add(game);

            MainActivity.mRequestQueue.add(requestGameInfo);

    }
}

package gamebooks.gamebooks.app.Exceptions;

/**
 * Wird geworfen, wenn das Backend mit einem unerwarteten Http Response Code antwortet
 */
public class BackendCommunicationException extends Exception {

    public BackendCommunicationException (String errorMsg) {
        super(errorMsg);
    }

}